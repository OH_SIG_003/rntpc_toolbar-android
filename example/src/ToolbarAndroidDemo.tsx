/*
 * Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
 * Use of this source code is governed by a MIT license that can be
 * found in the LICENSE file.
 */

import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import ToolbarAndroid from "@react-native-community/toolbar-android";

function ToolbarAndroidDemo({}): JSX.Element {
  const [state, setState] = useState<{
    message?: string;
  }>({
    message: undefined,
  });

  const { message } = state;

  return (
    <View style={styles.container}>
      <ToolbarAndroid
        navIcon={require("./assets/app_icon.png")}
        logo={require("./assets/icon.png")}
        title={"ToolbarAndroid Example"}
        subtitle={"ToolbarAndroid Subtitle"}
        titleColor={"#FFFFFF"}
        subtitleColor={"#FF00FF"}
        contentInsetStart={10}
        contentInsetEnd={10}
        rtl={false}
        style={styles.toolbar}
        actions={[
          {
            title: "Action1",
            icon: require("./assets/app_icon.png"),
            show: "ifRoom",
            showWithText: true,
          },
        ]}
        overflowIcon={require("./assets/app_icon.png")}
        onIconClicked={() => setState({ message: "Clicked nav icon" })}
        onActionSelected={(position: number) =>
          setState({ message: `Clicked Menu-${position}` })
        }
      />
      <View style={styles.centerContainer}>
        <Text style={styles.headText}>
          Click nav or action icon will trigger some events!
        </Text>
        <Text style={styles.contentText}>{message}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  toolbar: {
    backgroundColor: "#E9EAED",
    height: 56,
  },
  centerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  headText: {
    fontSize: 16,
  },
  contentText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#ff681f",
  },
});

export default ToolbarAndroidDemo;